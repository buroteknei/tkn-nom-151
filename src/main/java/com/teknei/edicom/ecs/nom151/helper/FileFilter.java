package com.teknei.edicom.ecs.nom151.helper;

import java.io.File;
import java.io.FilenameFilter;

public class FileFilter
  implements FilenameFilter
{
  public static final String ASTERISK = "*";
  public static final String BLANK = "";
  String mascara = "";
  
  public FileFilter(String mascara) {
    this.mascara = mascara.toUpperCase();
  }
  







  private boolean cumpleFiltro(String nombre)
  {
    String maskName = FileUtils.removeFileExt(this.mascara);
    String maskExtension = "";
    if (!this.mascara.equals(maskName)) {
      maskExtension = this.mascara.substring(maskName.length() + 1);
    }
    String fileName = FileUtils.removeFileExt(nombre);
    String fileExtension = "";
    if (!nombre.equals(fileName))
      fileExtension = nombre.substring(fileName.length() + 1);
    boolean devolver;
    if ((fileExtension.equals("")) && (!maskExtension.equals("")) && (!maskExtension.equals("*"))) {
      devolver = false;
    } else {
      devolver = (hasPattern(fileName, maskName)) && (hasPattern(fileExtension, maskExtension));
    }
    return devolver;
  }
  
  private boolean hasPattern(String name, String pattern) {
    boolean devolver = false;
    
    int index = pattern.indexOf("*");
    if (index != -1)
    {
      String right = name.substring(0, index);
      if (name.startsWith(right))
      {
        String izda = pattern.substring(index + 1, pattern.length());
        if (name.endsWith(izda))
        {
          devolver = true;
        }
        
      }
    }
    else if (name.compareTo(pattern) == 0) {
      devolver = true;
    }
    
    return devolver;
  }
  

  public boolean accept(File dir, String name)
  {
    return cumpleFiltro(name.toUpperCase());
  }
}
