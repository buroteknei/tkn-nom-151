package com.teknei.edicom.ecs.nom151;

import com.teknei.edicom.ecs.nom151.client.Enom151Client;
import com.teknei.edicom.ecs.nom151.client.Enom151Exception;
import com.teknei.edicom.ecs.nom151.helper.ConstanciaData;
import com.teknei.edicom.ecs.nom151.helper.FileUtils;
import com.teknei.edicom.ecs.nom151.helper.GestorLogs;

//import enom151.ecs.edicom.com.EcsConstanciaResult;
import com.sanityinc.jargs.CmdLineParser;
import com.sanityinc.jargs.CmdLineParser.Option;
import com.sanityinc.jargs.CmdLineParser.OptionException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
public class VerifyConstancia extends Enom151Program
{
  VerifyConstancia() throws Enom151Exception
  {}
  private static final Logger log = LoggerFactory.getLogger(VerifyConstancia.class);
  public void showHelp()
  {
	  log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".showHelp");
    StringBuilder buffer = new StringBuilder();
    buffer.append("VerifyConstancia: \n");
    buffer.append("\n");
    buffer.append("How to use: VerifyConstancia ");
    buffer.append("[{-").append('i').append(", --").append("in").append("} dirIn").append(", ");
    buffer.append("[{-").append('o').append(", --").append("out").append("} dirOut").append(", ");
    
    buffer.append("{-").append('u').append(", --").append("user").append("} user").append(", ");
    buffer.append("{-").append('p').append(", --").append("pass").append("} pass").append(", ");
    
    buffer.append("{-").append('c').append(", --").append("constancia").append("} constancia").append(", ");
    buffer.append("{-").append('f').append(", --").append("file").append("} file").append(", ");
    buffer.append("{-").append('e').append(", --").append("idExt").append("} idExt").append(", ");
    
    buffer.append("{-").append('r').append(", --").append("deleteInputFiles").append("} deleteInputFiles]");
    
    GestorLogs.info(VerifyConstancia.class, "Help", buffer.toString());
  }
  
  void addArguments(String[] args)
    throws Enom151Exception
  {
	  log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".addArguments");
    CmdLineParser parser = new CmdLineParser();
    
    Option<String> url = parser.addStringOption('l', "url");
    Option<String> pfx = parser.addStringOption('s', "pfx");
    Option<String> key = parser.addStringOption('k', "key");
    
    Option<String> dirIn = parser.addStringOption('i', "in");
    Option<String> dirOut = parser.addStringOption('o', "out");
    
    Option<String> user = parser.addStringOption('u', "user");
    Option<String> pass = parser.addStringOption('p', "pass");
    
    Option<String> constancia = parser.addStringOption('c', "constancia");
    Option<String> file = parser.addStringOption('f', "file");
    Option<String> idExt = parser.addStringOption('e', "idExt");
    
    Option<String> delete = parser.addStringOption('r', "deleteInputFiles");
    try
    {
      parser.parse(args, java.util.Locale.getDefault());
    } catch (OptionException optionException) {
      String texto = "Error while processing client parameters: ";
      GestorLogs.error(VerifyConstancia.class, "Add Arguments", texto + optionException.toString(), optionException);
      showHelp();
    }
    
    String urlValue = (String)parser.getOptionValue(url);
    insertaValorProperties("url", urlValue);
    
    String pfxValue = (String)parser.getOptionValue(pfx);
    insertaValorProperties("pfx", pfxValue);
    
    String keyValue = (String)parser.getOptionValue(key);
    insertaValorProperties("key", keyValue);
    
    String dirInValue = (String)parser.getOptionValue(dirIn);
    insertaValorProperties("in", dirInValue);
    
    String dirOutValue = (String)parser.getOptionValue(dirOut);
    insertaValorProperties("out", dirOutValue);
    
    String userValue = (String)parser.getOptionValue(user);
    insertaValorProperties("user", userValue);
    
    String passValue = (String)parser.getOptionValue(pass);
    insertaValorProperties("pass", passValue);
    
    String constValue = (String)parser.getOptionValue(constancia);
    insertaValorProperties("constancia", constValue);
    
    String fileValue = (String)parser.getOptionValue(file);
    insertaValorProperties("file", fileValue);
    
    String idExtValue = (String)parser.getOptionValue(idExt);
    insertaValorProperties("idExt", idExtValue);
    
    String deleteValue = (String)parser.getOptionValue(delete);
    insertaValorProperties("deleteInputFiles", deleteValue);
  }
  
  boolean validaDatos() throws Enom151Exception
  {
	  log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".validaDatos");
    boolean validateIn = false;
    boolean validateOut = false;
    try {
      String inDir = getProperties().getProperty("in");
      File f = new File(inDir);
      String constancia = getProperties().getProperty("constancia");
      String file = getProperties().getProperty("file");
      if (((f.exists()) && (f.list().length > 0)) || ((StringUtils.isNotBlank(constancia)) && (StringUtils.isNotBlank(file)))) {
        validateIn = true;
      }
      String outDir = getProperties().getProperty("out");
      f = new File(outDir);
      if (f.exists()) {
        validateOut = true;
      }
    } catch (Exception exception) {
      GestorLogs.error(VerifyConstancia.class, "Exception", exception.getMessage(), exception);
    }
    return (validateIn) && (validateOut);
  }
  


  protected List<ConstanciaData> verifyConstancia()
    throws Enom151Exception
  {
	  log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".verifyConstancia");
    List<ConstanciaData> result = new ArrayList<ConstanciaData>();
    GestorLogs.info(VerifyConstancia.class, "VERIFY CONSTANCIA", "start");
    
    try
    {
      Enom151Client client = new Enom151Client(getProperties().getProperty("url"), getProperties().getProperty("user"), getProperties().getProperty("pass"));
      
      String dirIn = getProperties().getProperty("in");
      File fDirIn = new File(dirIn);
      String constanciaParameter = getProperties().getProperty("constancia");
      String fileParameter = getProperties().getProperty("file");
      if ((FileUtils.isFile(constanciaParameter)) && (FileUtils.isFile(fileParameter))) {
        result.add(new ConstanciaData(constanciaParameter, fileParameter));
      } else {
        if ((StringUtils.isBlank(constanciaParameter)) && (fDirIn.exists()) && (fDirIn.list().length > 0)) {
          String[] constanciasNames = FileUtils.getFiles(fDirIn.getAbsolutePath(), "*.asn", false);
          for (String constanciaName : constanciasNames) {
            String fileName = getFilenameFromConstanciaName(fDirIn.getAbsolutePath(), constanciaName);
            result.add(new ConstanciaData(fDirIn.getAbsolutePath() + File.separator + constanciaName, fileName));
          }
        }
        if (StringUtils.isNotBlank(constanciaParameter)) {
          result = getFileBytes(constanciaParameter, dirIn);
        }
      }
      
      if (result.isEmpty()) {
        throw new Enom151Exception("Error en la interpretación de los parámetros de entrada");
      }
      
      String idExt = getProperties().getProperty("idExt");
      
      for (Iterator<ConstanciaData> it = result.iterator(); ((Iterator<ConstanciaData>)it).hasNext();) {
		ConstanciaData constanciaData = (ConstanciaData)((Iterator<ConstanciaData>)it).next();
        if (constanciaData.getFileName() != null) {
          Path pathConstancia = Paths.get(constanciaData.getConstanciaName(), new String[0]);
          byte[] constanciaByte = Files.readAllBytes(pathConstancia);
          Path pathFile = Paths.get(constanciaData.getFileName(), new String[0]);
          byte[] fileByte = Files.readAllBytes(pathFile);
          constanciaData.setEcsConstanciaResult(client.verifyConstanciaFile(constanciaByte, fileByte, idExt));
        }
      } } catch (Exception exception) {
      GestorLogs.error(VerifyConstancia.class, "Client Exception", "The client could not be connected", exception);
    }
    return result;
  }
  
  public void verifyConstanciaController(String[] args) throws Enom151Exception 
  {
	  log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".verifyConstanciaController");
    try {
      if ((args.length != 0) && ((args[0].endsWith("help")) || 
        (args[0].endsWith(String.valueOf('h'))))) {
        showHelp();
        return;
      }
      addArguments(args);
      GestorLogs.info(VerifyConstancia.class, "main", "Parametros: " + getProperties().toString());
      String dirOut; if ((verifyProperties()) && (validaDatos())) {
        List<ConstanciaData> verifyConstancias = verifyConstancia();
        dirOut = getProperties().getProperty("out");
        for (ConstanciaData constanciaData : verifyConstancias) {
          String newDirOut = generateOutputDir(dirOut, constanciaData.getConstanciaName());
          StringBuilder sb = new StringBuilder();
          sb.append("TimestampInfo: \n");
          sb.append(constanciaData.getEcsConstanciaResult().getTimestampInfo()).append("\n");
          sb.append("Fecha: " + constanciaData.getEcsConstanciaResult().getFechaInicioVigencia().toString()).append("\n\n");
          sb.append("Hash: " + constanciaData.getEcsConstanciaResult().getHash()).append("\n\n");
          sb.append("Resultado: " + constanciaData.getEcsConstanciaResult().isResult());
          String bytes = sb.toString();
          
          Path pathConstancia = Paths.get(constanciaData.getConstanciaName(), new String[0]);
          generatedOutputFromContent(Files.readAllBytes(pathConstancia), constanciaData.getConstanciaName(), null, newDirOut, "VerifyConstancia", VerifyConstancia.class);
          
          Path pathFile = Paths.get(constanciaData.getFileName(), new String[0]);
          generatedOutputFromContent(Files.readAllBytes(pathFile), constanciaData.getFileName(), null, newDirOut, "VerifyConstancia", VerifyConstancia.class);
          
          generatedOutputFromContent(bytes.getBytes(), constanciaData.getFileName(), ".status", newDirOut, "VerifyConstancia", VerifyConstancia.class);
          
          GestorLogs.info(VerifyConstancia.class, "main", "Resultado: \n" + bytes + "\n");
        }
      } else {
        GestorLogs.error(VerifyConstancia.class, "VerifyConstancia", Enom151Exception.getTextCode(5));
      }
      deleteInputFiles();
      GestorLogs.info(VerifyConstancia.class, "main", "Terminated!");
    } catch (IOException ex) {
      GestorLogs.error(VerifyConstancia.class, "main", ex.getMessage(), ex);
    }
  }
}

