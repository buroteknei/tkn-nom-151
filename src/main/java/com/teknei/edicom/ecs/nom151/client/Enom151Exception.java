package com.teknei.edicom.ecs.nom151.client;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Enom151Exception
  extends Exception
{
  private static final Logger log = LoggerFactory.getLogger(Enom151Exception.class);
  private static final long serialVersionUID = 1L;
  
  public static final int ERROR_DESCONOCIDO = -1;
  
  public static final int ERROR_LOG4J = 1;
  
  public static final int ERROR_PROPERTIES = 2;
  public static final int ERROR_PARAMETROS = 3;
  public static final int ERROR_PROCESO = 4;
  public static final int ERROR_DOCUMENT = 5;
  protected int codException;
  protected String textException;
  
  public Enom151Exception()
  {
	  log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".Enom151Exception 1");
    setCodException(-1);
    setTextException("ERROR");
  }
  





  public Enom151Exception(String message)
  {
	  log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".Enom151Exception 2");
    super(message);
    setCodException(-1);
    setTextException(message);
  }
  







  public Enom151Exception(int codException, String message)
  {
	  log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".Enom151Exception");
    super(message);
    setCodException(codException);
    setTextException(message);
  }
  




  public int getCodException()
  {
    return this.codException;
  }
  
  private void setCodException(int codException) {
    this.codException = codException;
  }
  




  public String getTextException()
  {
    return this.textException;
  }
  
  private void setTextException(String textException) {
    this.textException = textException;
  }
  




  public String getTextCode()
  {
	  log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".getTextCode");
    return getTextCode(getCodException());
  }
  






  public static String getTextCode(int codeException)
  {
	  log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".getTextCode int");
    String s = "ERROR";
    switch (codeException) {
    case -1: 
      s = "Unknown error.";
      break;
    case 1: 
      s = "It was not possible to initialize the client log system.";
      break;
    case 2: 
      s = "Could not load client property file.";
      break;
    case 3: 
      s = "Error in the parameters needed to invoke the service.";
      break;
    case 4: 
      s = "There was an error invoking the service.";
      break;
    case 5: 
      s = "There was an error generating the document.";
      break;
    }
    
    
    return s;
  }
}
