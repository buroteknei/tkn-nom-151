package com.teknei.edicom.ecs.nom151;

import com.teknei.edicom.ecs.nom151.client.Enom151Client;
import com.teknei.edicom.ecs.nom151.client.Enom151Exception;
import com.teknei.edicom.ecs.nom151.helper.FileUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Enom151WSClientView {
    protected static Logger logger = Logger.getLogger(Enom151WSClientView.class.getName()); 
    private String[] parameters;

    private String operation;


    public String[] getParameters() {
        return this.parameters;
    }

    public void setParameters(String[] parameters) {
        this.parameters = parameters;
    }

    public String getOperation() {
        return this.operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    void launchOperation(String[] args) throws Enom151Exception {
    	log.info("lblancas:  [tkn-nom-151] :: "+this.getClass().getName()+".launchOperation");
        if (getOperation().equals("GetConstancia")) {
            GetConstancia getConstancia = new GetConstancia();
            getConstancia.getConstanciaController(args);
        } else if (getOperation().equals("GetConstanciaTSR")) {
            GetConstanciaTSR getConstanciaTSR = new GetConstanciaTSR();
            getConstanciaTSR.getConstanciaTSRController(args);
        } else if (getOperation().equals("CheckStatus")) {
            CheckStatus checkStatus = new CheckStatus();
            checkStatus.checkStatusController(args);
        } else if (getOperation().equals("VerifyConstancia")) {
            VerifyConstancia verifyConstancia = new VerifyConstancia();
            verifyConstancia.verifyConstanciaController(args);
        } else if (getOperation().equals("VerifyConstanciaTSR")) {
            VerifyConstanciaTSR verifyConstanciaTSR = new VerifyConstanciaTSR();
            verifyConstanciaTSR.verifyConstanciaTSRController(args);
        } else {
            showHelp();
        }
    }

    public void showHelp() 
    {
    	log.info("lblancas:  [tkn-nom-151] :: "+this.getClass().getName()+".showHelp");
        StringBuilder buffer = new StringBuilder();
        buffer.append("Enom151WSClientView \n");
        buffer.append("================= \n");
        buffer.append("How to use: Enom151WSClientView ");
        buffer.append("[Operation] (Parameters) \n\n");
        buffer.append("Where Operation must be one of the following: \n");
        buffer.append("\n");
        buffer.append("GetConstancia\n");
        buffer.append("GetConstanciaTSR\n");
        buffer.append("VerifyConstancia\n");
        buffer.append("VerifyConstanciaTSR\n");
        buffer.append("\n");
        buffer.append("And Parameters must be enter by means of a configuration file, see configuration folder for details, \nor specifying -h for explanation: Enom151WSClientView [Operation] -h.");


        String help = buffer.toString();

        logger.setLevel(Level.INFO);
        logger.info(help);
    }

    boolean validaDatos() throws Enom151Exception 
    {
    	log.info("lblancas:  [tkn-nom-151] :: "+this.getClass().getName()+".dataSource");
        if (getParameters().length == 0) {
            return false;
        }
        return true;
    }

    public static void main(String[] args) throws Exception 
    {
    	log.info("lblancas:  [tkn-nom-151] :: "+this.getClass().getName()+".main");
        String url = args[0];//"https://ecsweb.sedeb2b.com:9025/EdicomCryptoServer/services/enom151";
        //String url = "https://ecsweb.sedeb2b.com:9025/EdicomCryptoServer/services/enom151";
        String user = args[1];//"BID170328LY0_SBDL";
        //String user = "BID170328LY0_SBDL";
        String pass = args[2];//"shmkaua3k4";
        //String pass = "shmkaua3k4";
        String idExt = "IDExterno";
        Enom151Client client = new Enom151Client(url, user, pass);
        //byte[] fileContent = Files.readAllBytes(Paths.get("/home/amaro/Documentos/TKN/FirmaContratoSabadell/PDF/salida_signed_client.pdf"));
        byte[] fileContent = Files.readAllBytes(Paths.get(args[3]));
        //PDDocument pdDocument = PDDocument.load(fileContent);
        //PDFTextStripper stripper = new PDFTextStripper();
        //String text = stripper.getText(pdDocument);
        //String hash = FileUtils.getSha256Timestamp(text);
        byte[] constPDF = client.getConstanciaAsPdf(fileContent, idExt);
        //byte[] constHash = client.getConstanciaHash(hash, idExt);
        //byte[] constFile = client.getConstanciaFile(fileContent, idExt);
        String constUri = args[3];
        constUri = constUri.substring(0, constUri.length() - 4);
        constUri = constUri + "_const_hash.pdf";
        //String constUri = args[3].replace("signed_client.pd", "const_hash.asn");
        //String constUri = "/home/amaro/Documentos/TKN/FirmaContratoSabadell/PDF/salida_signed_client.pdf".replace("signed_client.pdf", "const_hash.asn");
        //String constUriPDF = "/home/amaro/Documentos/TKN/FirmaContratoSabadell/PDF/salida_signed_client.pdf".replace("signed_client.pdf", "const_hash_psd.asn");
        //String constUriFile = "/home/amaro/Documentos/TKN/FirmaContratoSabadell/PDF/salida_signed_client.pdf".replace("signed_client.pdf", "const_hash_file.asn");
        //Files.write(Paths.get(constUri), constHash);
        Files.write(Paths.get(constUri), constPDF);
        //Files.write(Paths.get(constUriFile), constFile);
    }

  /*
  public static void main(String[] args) throws Enom151Exception {
    Enom151WSClientView view = new Enom151WSClientView();
    view.setParameters(args);
    if (!view.validaDatos()) {
      view.showHelp();
    } else {
      view.setOperation(args[0]);
      String[] argsNew;
	if (args.length == 1) {
        argsNew = new String[0];
      } else {
        argsNew = (String[])Arrays.copyOfRange(args, 1, args.length);
      }
      view.launchOperation(argsNew);
    }
  }*/
}
