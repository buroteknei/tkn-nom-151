package com.teknei.edicom.ecs.nom151.helper;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;




public class FileUtils
{
  public static final String ESCRITURA_FICHERO = "Escritura Fichero";
  public static final int TAMBUF = 1048576;
  public static final String BAR = "/";
  public static final String BLANK = "";
  public static final String TWO_INVERSE_BAR = "\\";
  public static final String TWO_POINT = "..";
  public static final String POINT = ".";
  
  public static String[] getDirFiles(String path)
  {
    boolean recursive = false;
    return getDirFiles(path, recursive);
  }
  
  public static String[] getDirFiles(String path, boolean recursive) { String mask = "*.";
    return getFiles(path, mask, recursive);
  }
  
  public static String[] getFiles(String path) { String mask = "*";
    return getFiles(path, mask);
  }
  
  public static String[] getFiles(String path, String mask) { boolean recursive = false;
    return getFiles(path, mask, recursive);
  }
  
  public static String[] getFiles(String path, String mask, boolean recursive)
  {
    List<String> array = new ArrayList<String>();
    File f = new File(path);
    if ((f.exists()) && (f.isDirectory())) {
      String[] files = f.list(isEmpty(mask) ? null : new FileFilter(mask));
      for (int i = 0; i < files.length; i++) {
        String fileName = files[i];
        if ((!fileName.equals(".")) && (!fileName.equals(".."))) {
          array.add(fileName);
          if (recursive) {
            File f1 = new File(fileName);
            if (f1.isDirectory()) {
              String[] subdirFiles = getFiles(f1.getAbsolutePath(), mask, recursive);
              for (int j = 0; j < subdirFiles.length; j++) {
                String subFile = subdirFiles[j];
                if ((!".".equals(subFile)) && (!"..".equals(subFile))) {
                  array.add(subFile);
                }
              }
            }
          }
        }
      }
    }
    String[] files = new String[array.size()];
    return (String[])array.toArray(files);
  }
  
  public static boolean isEmpty(String mask) {
    if (StringUtils.isEmpty(mask)) {
      return true;
    }
    if ("*".equals(mask)) {
      return true;
    }
    if (mask.endsWith(".*")) {
      return true;
    }
    return false;
  }
  
  public static String preparaRutaDirectorio(String path) {
    String s = path;
    if (s.indexOf("\\") >= 0) {
      s = StringUtils.reemplazaCadena(s, "\\", "/");
    }
    if (!s.endsWith("/")) {
      s = s + "/";
    }
    return s;
  }
  
  public static String getFileNameWithoutExtension(String fileName) {
    String fileNameName = getFileName(fileName);
    String extension = getFileExtension(fileName);
    if (!extension.equals("")) {
      fileNameName = getFilePath(fileNameName, ".");
    }
    return fileNameName;
  }
  
  public static String getFileExtension(String fileName) { boolean before = false;
    return getFilePath(fileName, ".", before);
  }
  
  public static String changeFileExt(String archivo, String newExt) {
    String newFileName = getFileNameWithoutExtension(archivo);
    if ((newExt != null) && (!newExt.equals(""))) {
      newFileName = newFileName + "." + newExt;
    }
    return newFileName;
  }
  
  public static String removeFileExt(String archivo) {
    return getFilePath(archivo, ".");
  }
  
  public static String getFileDir(String archivo) {
    return getFilePath(archivo, "/");
  }
  
  private static String getFilePath(String archivo, String searchMask) {
    boolean before = true;
    return getFilePath(archivo, searchMask, before);
  }
  
  private static String getFilePath(String archivo, String searchMask, boolean before) { String filePath = StringUtils.reemplazaCadena(archivo, "\\", "/");
    String path = filePath;
    int index = archivo.lastIndexOf(searchMask);
    if (index != -1) {
      int initIndex = 0;
      int endIndex = index;
      if (!before) {
        initIndex = index + 1;
        endIndex = filePath.length();
      }
      path = archivo.substring(initIndex, endIndex);
    }
    return path;
  }
  
  public static String getFileName(String archivo) {
    boolean before = false;
    return getFilePath(archivo, "/", before);
  }
  





  public static byte[] lecturaFicheroBinario(String fich)
  {
    File fichero = new File(fich);
    return lecturaFicheroBinario(fichero);
  }
  




  public static byte[] lecturaFicheroBinario(File fich)
  {
    byte[] datos = new byte[0];
    FileInputStream fis = null;
    try { 
    	ByteArrayOutputStream baos = null;
      if ((fich != null) && (fich.exists())) {
        fis = new FileInputStream(fich);
        int availableLength = fis.available();
        if (availableLength < 1048576) {
          datos = new byte[availableLength];
          fis.read(datos);
        } else {
          baos = new ByteArrayOutputStream();
          byte[] buffer = new byte[1048576];
          int count = 0;
          do {
            count = fis.read(buffer, 0, buffer.length);
            baos.write(buffer);
          } while (count != -1); } }
      return baos.toByteArray();
    }
    catch (FileNotFoundException fnfException)
    {
      GestorLogs.error(FileUtils.class, "Lectura Fichero Binario", "No existe el fichero " + fich.getAbsolutePath(), fnfException);
    } catch (IOException ioException) {
      GestorLogs.error(FileUtils.class, "Lectura Fichero Binario", "Error al leer el fichero " + fich.getAbsolutePath(), ioException);
    } finally {
      try {
        if (fis != null) {
          fis.close();
        }
      } catch (Exception exception) {
        GestorLogs.error(FileUtils.class, "Cierre Fichero Binario", "Error al cerrar el fichero " + fich.getAbsolutePath(), exception);
      }
    }
    return null;
  }
  







  public static String leerFichero(String fich)
  {
    File f1 = new File(fich);
    return lecturaFichero(f1);
  }
  





  private static String lecturaFichero(File fich)
  {
    StringBuilder devolver = new StringBuilder("");
    if (!fich.exists()) {
      return devolver.toString();
    }
    char[] buff = new char[1048576];
    

    BufferedReader br = null;
    FileInputStream fis = null;
    try {
      fis = new FileInputStream(fich);
      
      br = new BufferedReader(new InputStreamReader(fis, "ISO-8859-1"));
      String cadena = null;
      int nch; while ((nch = br.read(buff, 0, buff.length)) != -1) {
        cadena = new String(buff, 0, nch);
        devolver.append(cadena);
      }
      cadena = null;
      















      return devolver.toString();
    }
    catch (Exception e)
    {
      GestorLogs.error(FileUtils.class, "Lectura Fichero", "No puedo leer del fichero " + fich.toString() + ": " + e.toString(), e);
    } finally {
      buff = null;
      try {
        if (br != null) {
          br.close();
        }
        if (fis != null) {
          fis.close();
        }
      } catch (Exception ex) {
        GestorLogs.error(FileUtils.class, "Cierre Fichero", "Error al cerrar el fichero " + fich.getAbsolutePath(), ex);
      }
    }
    return null;
  }
  










  public static void escribirFichero(String fich, byte[] datos, long tiempo)
  {
    File f1 = new File(fich);
    String dir = f1.getParent();
    
    File fDir = new File(dir);
    fDir.mkdirs();
    
    escrituraFichero(f1, datos);
    try {
      if ((tiempo != 0L) && (f1.exists())) {
        f1.setLastModified(tiempo);
      }
    } catch (Exception ex) {
      GestorLogs.error(FileUtils.class, "Escribir Fichero", "No puedo cambiar la fecha de creacion del fichero: " + ex.toString(), ex);
    }
  }
  







  private static void escrituraFichero(File fich, byte[] datos)
  {
    BufferedOutputStream out = null;
    FileOutputStream fis = null;
    boolean creado = false;
    try {
      if (!fich.exists()) {
        creado = fich.createNewFile();
        if (!creado) {
          GestorLogs.warn(FileUtils.class, "Escritura Fichero", "Intentamos escribir en un fichero que no se ha podido crear.\n Fichero = " + fich.toString());
        }
      }
      
      fis = new FileOutputStream(fich);
      out = new BufferedOutputStream(fis);
      out.write(datos); return;
    } catch (Exception e) {
      GestorLogs.error(FileUtils.class, "Escritura Fichero", "Error al escribir el fichero " + fich.toString() + ": " + e.toString(), e);
    } finally {
      try {
        if (out != null) {
          out.close();
        }
        if (fis != null) {
          fis.close();
        }
      } catch (Exception ex) {
        GestorLogs.error(FileUtils.class, "Cierre Fichero", "Error al cerrar el fichero " + fich.getAbsolutePath(), ex);
      }
    }
  }
  
  public static boolean borrarFichero(String fich) {
    File file = new File(fich);
    return borrarFichero(file);
  }
  
  public static boolean borrarFichero(File fich) {
    boolean b = false;
    if ((fich != null) && (fich.exists()) && (!fich.isDirectory())) {
      b = fich.delete();
    }
    return b;
  }
  
  public static String getSha256Timestamp(String fileContent) {
    try {
      MessageDigest digest = MessageDigest.getInstance("SHA-256");
      byte[] hash = digest.digest(fileContent.getBytes("UTF-8"));
      StringBuilder hexString = new StringBuilder();
      
      for (int i = 0; i < hash.length; i++) {
        String hex = Integer.toHexString(0xFF & hash[i]);
        if (hex.length() == 1) {
          hexString.append('0');
        }
        hexString.append(hex);
      }
      return hexString.toString();
    } catch (Exception ex) {
      GestorLogs.error(FileUtils.class, "GET SHA256 TIMESTAMP", "No se pudo generar el timestamp: " + ex.toString(), ex);
    }
    return "";
  }
  
  public static boolean isFile(String filename) {
    if (StringUtils.isEmpty(filename)) {
      return false;
    }
    File file = new File(filename);
    if (file.exists()) {
      return true;
    }
    return false;
  }
}
