package com.teknei.edicom.ecs.nom151.helper;

import enom151.ecs.edicom.com.EcsConstanciaResult;

public class ConstanciaData
{
  private String constanciaName;
  private String fileName;
  private EcsConstanciaResult ecsConstanciaResult;
  
  public ConstanciaData()
  {
    this.constanciaName = null;
    this.fileName = null;
    this.ecsConstanciaResult = null;
  }
  
  public ConstanciaData(String constanciaName, String fileName)
  {
    this.constanciaName = constanciaName;
    this.fileName = fileName;
    this.ecsConstanciaResult = null;
  }
  
  public String getConstanciaName() {
    return this.constanciaName;
  }
  
  public void setConstanciaName(String constanciaName) {
    this.constanciaName = constanciaName;
  }
  
  public String getFileName() {
    return this.fileName;
  }
  
  public void setFileName(String fileName) {
    this.fileName = fileName;
  }
  
  public EcsConstanciaResult getEcsConstanciaResult() {
    return this.ecsConstanciaResult;
  }
  
  public void setEcsConstanciaResult(EcsConstanciaResult ecsConstanciaResult) {
    this.ecsConstanciaResult = ecsConstanciaResult;
  }
}
