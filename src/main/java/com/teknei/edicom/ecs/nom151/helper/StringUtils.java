package com.teknei.edicom.ecs.nom151.helper;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.StringTokenizer;




public class StringUtils
{
  @SuppressWarnings("unused")
private static final String BLANK = "";
  
  public static String[] getCadenasComaLista(String cadena)
  {
    String separador = ",";
    return getCadenasLista(cadena, separador);
  }
  
  public static List<String> getCadenasArrayList(String cadena, String separador) {
    List<String> lista = new ArrayList<String>();
    if (cadena != null) {
      String newCadena = cadena;
      if (cadena.contains(separador))
      {
        newCadena = cadena.replaceAll(separador, "|");
      }
      StringTokenizer stk = new StringTokenizer(newCadena, "|");
      
      while (stk.hasMoreElements()) {
        String id = (String)stk.nextElement();
        if (!id.equals("")) {
          lista.add(id);
        }
      }
    }
    return lista;
  }
  
  public static String getArrayListCadenas(List<String> cadenas, String separador) { String cadena = "";
    if ((cadenas != null) && (separador != null)) {
      for (int i = 0; i < cadenas.size(); i++) {
        @SuppressWarnings("unused")
		StringBuilder sb = new StringBuilder();
        cadena = cadena + (String)cadenas.get(i);
        if (i < cadenas.size() - 1) {
          sb = new StringBuilder();
          cadena = cadena + separador;
        }
      }
    }
    return cadena;
  }
  
  public static String[] getCadenasLista(String cadena, String separador) {
    List<String> lista = getCadenasArrayList(cadena, separador);
    String[] listado = new String[lista.size()];
    listado = (String[])lista.toArray(listado);
    return listado;
  }
  
  public static String getListaCadenas(String[] cadenas, String separador) { String cadena = "";
    if ((cadenas != null) && (separador != null)) {
      for (int i = 0; i < cadenas.length; i++) {
        @SuppressWarnings("unused")
		StringBuilder sb = new StringBuilder();
        cadena = cadena + cadenas[i];
        if (i < cadenas.length - 1) {
          sb = new StringBuilder();
          cadena = cadena + separador;
        }
      }
    }
    return cadena;
  }
  
  public static String generaCadenaAleatoria(String caracteresPermitidos, int longitudCadena)
  {
    String s = "";
    Random r = new Random();
    for (int i = 0; i < longitudCadena; i++) {
      @SuppressWarnings("unused")
	StringBuilder sb = new StringBuilder();
      int index = r.nextInt(longitudCadena);
      s = s + String.valueOf(caracteresPermitidos.charAt(index));
    }
    return s;
  }
  
  public static boolean isEmpty(String cadena) {
    return org.apache.commons.lang.StringUtils.isBlank(cadena);
  }
  







  public static String reemplazaCadena(String text, String searchString, String replacement)
  {
    if ((text == null) || (text.length() == 0) || (searchString == null) || (searchString.length() == 0)) {
      return text;
    }
    int start = 0;
    int end = text.indexOf(searchString, start);
    if (end == -1) {
      return text;
    }
    int replLength = searchString.length();
    int increase = replacement.length() - replLength;
    increase = increase < 0 ? 0 : increase;
    StringBuilder buf = new StringBuilder(text.length() + increase);
    while (end != -1) {
      buf.append(text.substring(start, end)).append(replacement);
      start = end + replLength;
      end = text.indexOf(searchString, start);
    }
    buf.append(text.substring(start));
    return buf.toString();
  }
}
