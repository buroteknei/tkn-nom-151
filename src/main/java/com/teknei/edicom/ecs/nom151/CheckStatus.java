package com.teknei.edicom.ecs.nom151;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.teknei.edicom.ecs.nom151.client.Enom151Client;
import com.teknei.edicom.ecs.nom151.client.Enom151Exception;
import com.teknei.edicom.ecs.nom151.helper.GestorLogs;

import com.sanityinc.jargs.CmdLineParser;
import com.sanityinc.jargs.CmdLineParser.Option;
import com.sanityinc.jargs.CmdLineParser.OptionException;
import java.util.Locale;
import org.apache.commons.lang.StringUtils;

public class CheckStatus
  extends Enom151Program
{
  private static final Logger log = LoggerFactory.getLogger(CheckStatus.class);
  CheckStatus() throws Enom151Exception
  {}
  
  public void showHelp()
  {
	log.info("lblancas:  [tkn-nom-151] :: "+this.getClass().getName()+".showHelp");
    StringBuilder buffer = new StringBuilder();
    buffer.append("CheckStatus: \n");
    buffer.append("\n");
    buffer.append("How to use: CheckStatus ");
    buffer.append("{-").append('u').append(", --").append("user").append("} user").append(", ");
    buffer.append("{-").append('p').append(", --").append("pass").append("} pass").append(", ");
    
    buffer.append("{-").append('q').append(", --").append("checkType").append("} checkType]");
    
    GestorLogs.info(CheckStatus.class, "Help", buffer.toString());
  }
  
  void addArguments(String[] args)
    throws Enom151Exception
  {
	  log.info("lblancas:  [tkn-nom-151] :: "+this.getClass().getName()+".addArguments");
    CmdLineParser parser = new CmdLineParser();
    
    Option<String> url = parser.addStringOption('l', "url");
    Option<String> pfx = parser.addStringOption('s', "pfx");
    Option<String> key = parser.addStringOption('k', "key");
    
    Option<String> user = parser.addStringOption('u', "user");
    Option<String> pass = parser.addStringOption('p', "pass");
    
    Option<String> type = parser.addStringOption('q', "checkType");
    try
    {
      parser.parse(args, Locale.getDefault());
    } catch (OptionException optionException) {
      String texto = "Error while processing client parameters: ";
      GestorLogs.error(CheckStatus.class, "Add Arguments", texto + optionException.toString(), optionException);
      showHelp();
    }
    
    String urlValue = (String)parser.getOptionValue(url);
    String pfxValue = (String)parser.getOptionValue(pfx);
    String keyValue = (String)parser.getOptionValue(key);
    
    String userValue = (String)parser.getOptionValue(user);
    String passValue = (String)parser.getOptionValue(pass);
    
    String typeValue = (String)parser.getOptionValue(type);
    
    insertaValorProperties("url", urlValue);
    insertaValorProperties("pfx", pfxValue);
    insertaValorProperties("key", keyValue);
    
    insertaValorProperties("user", userValue);
    insertaValorProperties("pass", passValue);
    
    insertaValorProperties("checkType", typeValue);
  }
  
  boolean validaDatos()
    throws Enom151Exception
  {
	log.info("lblancas:  [tkn-nom-151] :: "+this.getClass().getName()+".validaDatos");
    boolean validateIn = false;
    try {
      String type = getProperties().getProperty("checkType");
      if (StringUtils.isNumeric(type)) {
        validateIn = true;
      }
    } catch (Exception exception) {
      GestorLogs.error(CheckStatus.class, "Exception", exception.getMessage(), exception);
    }
    return validateIn;
  }
  


  protected boolean checkStatus()
    throws Enom151Exception
  {
	log.info("lblancas:  [tkn-nom-151] :: "+this.getClass().getName()+".checkStatus");
    boolean status = false;
    GestorLogs.info(CheckStatus.class, "CHECK STATUS", "start");
    
    try
    {
      Enom151Client client = new Enom151Client(getProperties().getProperty("url"), getProperties().getProperty("user"), getProperties().getProperty("pass"));
      
      int type = Integer.parseInt(getProperties().getProperty("checkType"));
      
      status = client.checkStatus(type);
    } catch (Exception exception) {
      GestorLogs.error(CheckStatus.class, "Client Exception", "The client could not be connected", exception);
    }
    return status;
  }
  
  public void checkStatusController(String[] args) throws Enom151Exception
  {
	  log.info("lblancas:  [tkn-nom-151] :: "+this.getClass().getName()+".checkStatusController");
	  try {
      if ((args.length != 0) && ((args[0].endsWith("help")) || 
        (args[0].endsWith(String.valueOf('h'))))) {
        showHelp();
        return;
      }
      addArguments(args);
      GestorLogs.info(CheckStatus.class, "main", "Parametros: " + getProperties().toString());
      if ((verifyProperties()) && (validaDatos())) {
        boolean status = checkStatus();
        GestorLogs.info(CheckStatus.class, "CheckStatus", "Respuesta: " + status);
      } else {
        GestorLogs.error(CheckStatus.class, "CheckStatus", Enom151Exception.getTextCode(5));
      }
      GestorLogs.info(CheckStatus.class, "main", "Terminated!");
    } catch (Exception ex) {
      GestorLogs.error(CheckStatus.class, "main", ex.getMessage(), ex);
    }
  }
}
