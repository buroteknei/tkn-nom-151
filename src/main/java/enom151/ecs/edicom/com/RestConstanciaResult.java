
package enom151.ecs.edicom.com;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para restConstanciaResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="restConstanciaResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="constanciaFechaInicioVigencia" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="constanciaHash" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="constanciaResult" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="constanciaTimeStampInfo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="errors" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="PDFSignatureValid" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="valid" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="warnings" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="xmlDiagnosticData" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "restConstanciaResult", propOrder = {
    "constanciaFechaInicioVigencia",
    "constanciaHash",
    "constanciaResult",
    "constanciaTimeStampInfo",
    "errors",
    "pdfSignatureValid",
    "valid",
    "warnings",
    "xmlDiagnosticData"
})
public class RestConstanciaResult {

    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar constanciaFechaInicioVigencia;
    protected String constanciaHash;
    protected boolean constanciaResult;
    protected String constanciaTimeStampInfo;
    @XmlElement(nillable = true)
    protected List<String> errors;
    @XmlElement(name = "PDFSignatureValid")
    protected boolean pdfSignatureValid;
    protected boolean valid;
    @XmlElement(nillable = true)
    protected List<String> warnings;
    protected String xmlDiagnosticData;

    /**
     * Obtiene el valor de la propiedad constanciaFechaInicioVigencia.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getConstanciaFechaInicioVigencia() {
        return constanciaFechaInicioVigencia;
    }

    /**
     * Define el valor de la propiedad constanciaFechaInicioVigencia.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setConstanciaFechaInicioVigencia(XMLGregorianCalendar value) {
        this.constanciaFechaInicioVigencia = value;
    }

    /**
     * Obtiene el valor de la propiedad constanciaHash.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConstanciaHash() {
        return constanciaHash;
    }

    /**
     * Define el valor de la propiedad constanciaHash.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConstanciaHash(String value) {
        this.constanciaHash = value;
    }

    /**
     * Obtiene el valor de la propiedad constanciaResult.
     * 
     */
    public boolean isConstanciaResult() {
        return constanciaResult;
    }

    /**
     * Define el valor de la propiedad constanciaResult.
     * 
     */
    public void setConstanciaResult(boolean value) {
        this.constanciaResult = value;
    }

    /**
     * Obtiene el valor de la propiedad constanciaTimeStampInfo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConstanciaTimeStampInfo() {
        return constanciaTimeStampInfo;
    }

    /**
     * Define el valor de la propiedad constanciaTimeStampInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConstanciaTimeStampInfo(String value) {
        this.constanciaTimeStampInfo = value;
    }

    /**
     * Gets the value of the errors property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the errors property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getErrors().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getErrors() {
        if (errors == null) {
            errors = new ArrayList<String>();
        }
        return this.errors;
    }

    /**
     * Obtiene el valor de la propiedad pdfSignatureValid.
     * 
     */
    public boolean isPDFSignatureValid() {
        return pdfSignatureValid;
    }

    /**
     * Define el valor de la propiedad pdfSignatureValid.
     * 
     */
    public void setPDFSignatureValid(boolean value) {
        this.pdfSignatureValid = value;
    }

    /**
     * Obtiene el valor de la propiedad valid.
     * 
     */
    public boolean isValid() {
        return valid;
    }

    /**
     * Define el valor de la propiedad valid.
     * 
     */
    public void setValid(boolean value) {
        this.valid = value;
    }

    /**
     * Gets the value of the warnings property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the warnings property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWarnings().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getWarnings() {
        if (warnings == null) {
            warnings = new ArrayList<String>();
        }
        return this.warnings;
    }

    /**
     * Obtiene el valor de la propiedad xmlDiagnosticData.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXmlDiagnosticData() {
        return xmlDiagnosticData;
    }

    /**
     * Define el valor de la propiedad xmlDiagnosticData.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXmlDiagnosticData(String value) {
        this.xmlDiagnosticData = value;
    }

}
